import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

  constructor(props) {
    super(props);
    this.state = {number:0}
    this.plus = this.plus.bind(this);
    this.minus = this.minus.bind(this);
    this.multi = this.multi.bind(this);
    this.divide = this.divide.bind(this);

  }

  plus() {
    this.setState({number:this.state.number+1})
    console.log("add");
  }
  minus() {
    this.setState({number:this.state.number-1})
  }
  multi() {
    this.setState({number:this.state.number*2})
  }
  divide() {
    this.setState({number:this.state.number/2})
  }

  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.number} </span>
        </div>
        <div className="operations">
          <button onClick={this.plus}>加1</button>
          <button onClick={this.minus}>减1</button>
          <button onClick={this.multi}>乘以2</button>
          <button onClick={this.divide}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

